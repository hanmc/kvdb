/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.8
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package org.kvdb;

public class Access {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Access(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Access obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        kvdbAccessJNI.delete_Access(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Access() {
    this(kvdbAccessJNI.new_Access(), true);
  }

  public void close() {
    kvdbAccessJNI.Access_close(swigCPtr, this);
  }

  public boolean open(String conf_path) {
    return kvdbAccessJNI.Access_open(swigCPtr, this, conf_path);
  }

  public boolean write(byte[] key_in, byte[] value_in) {
    return kvdbAccessJNI.Access_write(swigCPtr, this, key_in, value_in);
  }

  public boolean update(byte[] key_in, byte[] value_in) {
    return kvdbAccessJNI.Access_update(swigCPtr, this, key_in, value_in);
  }

  public long read_prepare(byte[] key_in, byte[] vptr) {
    return kvdbAccessJNI.Access_read_prepare(swigCPtr, this, key_in, vptr);
  }

  public long read_value(byte[] vptr, byte[] value_out) {
    return kvdbAccessJNI.Access_read_value(swigCPtr, this, vptr, value_out);
  }

  public boolean remove(byte[] key_in) {
    return kvdbAccessJNI.Access_remove(swigCPtr, this, key_in);
  }

}
