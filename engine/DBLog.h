#ifndef _ENGINE_DBLOG_H_
#define _ENGINE_DBLOG_H_

#include "BaseType.h"

#include "Util.h"

#include "Configuration.h"

#include <mutex>

namespace kv_engine {
using std::mutex;

class MemTable;

class DBLog {
public:
    DBLog(Configuration * conf) : _conf(conf){ }

    ~DBLog();

    Status Log(const KeyType & key, const ValueType & value);
    
    Status Open(const long & _id);

    static Status Recover(MemTable * _mem, long id, Configuration * conf);
    
private:
    Configuration * _conf = nullptr;

    int _fd = -1; // log file

    long _id;

    size_t _size = 0;
    
    mutex _size_mtx;

};

}
#endif // _ENGINE_DBLOG_H_